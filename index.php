<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Advocacia Criminal: Ariela Rodrigues</title>
    <link rel="stylesheet" href="bower_components/normalize.css/normalize.css">
    <link rel="stylesheet" href="bower_components/milligram/dist/milligram.min.css">
    <link rel="stylesheet" href="bower_components/milligram/dist/milligram.min.css.map">
    <link rel="stylesheet" href="public/css/site.css">
    <META NAME="description" CONTENT="Advogada criminal: acompanhamento de Inquérito Policial, andamento de Processo, Execução da Pena, Revisão Criminal, Defesas, Recursos, Sustentação Oral. Acompanhamento em delegacias, prisão em flagrante, prisão preventiva, apreensões, flagrante de ato infracional e mais.">
    <META NAME="keywords" CONTENT="advogado criminal, criminalista, processo penal, processo criminal, acompanhamento em flagrante, acompanhamento em delegacia, advogado, advogada, acompanhamento em audiência de custódia, plantão criminal 24 horas, kobrassol, florianópolis, são josé, grande florianópolis">
    <META NAME="robot" CONTENT="index,follow">
    <META NAME="language" CONTENT="PT_BR">
    <META NAME="revisit-after" CONTENT="30 days">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.png"/>
</head>
<body>

    <div id="wrap">
    
    <section id="header">
        <article class="container">
            <div class="row">
                <div class="column">
                    <div class="logo">
                        <h1>Ariela Rodrigues</h1>
                        <h2>Advocacia Criminal</h2>
                    </div>
                </div>
            </div>
        </article>
    </section>
    
    <nav id="primary-nav">
       <div class="container">
           <div class="row">
               <div class="column">
                   <a class="logo" href="#header">
                        <h1>Ariela Rodrigues</h1>
                        <h2>Advocacia Criminal</h2>
                    </a>
               </div>
               <div class="column">
                   <ul>
                        <li><a href="#servicos">Serviços</a></li>
                        <li><a href="#perfil">Perfil</a></li>
                        <li><a href="#blog">Blog</a></li>
                        <li><a href="#contato">Contato</a></li>
                    </ul>
               </div>
           </div>
       </div>
    </nav>
    
    <section id="servicos">
        <article class="container">
            <div class="row">
                <header class="column">
                    <h2 class="cms-editable" id="servicos-h2-titulo">Especialistas em <strong>Advocacia Criminal</strong></h2>
                </header>
            </div>
            
            <div class="row darker">
                <div class="column">
                    <img id="servicos-col1-img" class="cms-editable" src="public/img/img2.jpg" alt="">
                    <h3 id="servicos-col1-h3" class="cms-editable">Nos momentos <strong>mais difíceis</strong></h3>
                    <div id="servicos-col1-txt" class="cms-editable">
                        <p>Nosso atendimento é 24h por dia. Estaremos presentes em delegacias, prisão em flagrante, prisão preventiva, apreensões, flagrante de ato infracional. </p>
                        <p>Estaremos ao seu lado: pode contar com a gente.</p>
                    </div>
                    <p class="align-center">
                        <a href="#contato" id="servicos-col1-actionbtn" class="cms-editable button">Contratar agora!</a>
                    </p>
                </div>
                <div class="column">
                    <img id="servicos-col2-img" class="cms-editable" src="public/img/img3.jpg" alt="">
                    <h3 id="servicos-col2-h3" class="cms-editable">Para quem <strong>mais precisa</strong></h3>
                    <div id="servicos-col2-txt" class="cms-editable"> 
                        <p>Nós estamos preparados para atender o seu perfil:</p> 
                            <ul>
                                <li>Menor infrator</li>
                                <li>Pessoa física</li>
                                <li>Pessoa jurídica</li>
                            </ul>
                        <p>Podemos auxiliá-lo pessoalmente, auxiliar a sua empresa ou mesmo quem você ama.</p>
                    </div>
                    <p class="align-center">
                        <a href="#contato" id="servicos-col2-actionbtn" class="cms-editable button">Contratar agora!</a>
                    </p>
                </div>
                <div class="column">
                    <img id="servicos-col3-img" class="cms-editable" src="public/img/img1.jpg" alt="">
                    <h3 id="servicos-col3-h3" class="cms-editable">Serviços <strong>confiáveis</strong></h3>
                    <div id="servicos-col3-txt" class="cms-editable">
                        <p>Estaremos com você no acompanhamento ao Fórum garantindo seus direitos.</p>
                        <p>Temos acompanhamento processual: acompanhamento de Inquérito Policial, andamento de Processo, Execução da Pena, Revisão Criminal, Defesas, Recursos, Sustentação Oral. Acompanhamos você em todas as esferas.</p>
                    </div>
                    <p class="align-center">
                        <a href="#contato" id="servicos-col3-actionbtn" class="cms-editable button">Contratar agora!</a>
                    </p>
                </div>
            </div>
        </article>
    </section>
    
    <section id="perfil">
        <article class="container">
            <div class="row">
                <div class="column column-50">
                    <h2 id="perfil-h2" class="cms-editable">Profissionais gabaritados</h2>
                    
                    <h3 id="perfil-h3" class="cms-editable">Ariela Melo Rodrigues <span>OAB/SC 40.432</span></h3>
                   
                    <div id="perfil-text" class="cms-editable">
                        <p>Formada em Direito pela Universidade Federal de Santa Catarina, possui grande experiência com atuação concentrada em Processos Criminais Comuns, Juizado Especial Criminal e Execuções Penais.</p>

                        <p>Membro da Associação dos Advogados Criminalistas de Santa Catarina (AACRIMESC).</p>    
                    </div>
                    
                    <a href="#contato" id="perfil-actionbtn" class="cms-editable button">Contratar agora!</a>
                </div>
            </div>
        </article>
    </section>
    
    <section id="blog">
        <div class="container">
            <header>
                <h3 class="cms-editable" id="blog-h3">Por dentro da <strong>Advocacia Criminal</strong></h3>
            </header>
            
            <div id="rssOutput" class="rss-content"><a href="http://arielarodrigues.tumblr.com" target="_blank">Confira nosso blog e saiba das novidades!</a></div>
                       
            <div class="row">
                <div class="column">
                    <a href="http://arielarodrigues.tumblr.com/" id="blog-actionbtn" class="cms-editable button float-right">Conheça nosso blog</a>
                </div>
            </div>
        </div>
    </section>
    
    <section id="contato">
        <div class="container">
            <div class="row">
                <article class="column">
                    <div class="cms-editable" id="contato-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3536.042655601917!2d-48.614411684325745!3d-27.592207128531406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x952749d8ef25745f%3A0xec73a70332a8bac6!2sAriela+Melo+Rodrigues+Advocacia+Criminal!5e0!3m2!1spt-BR!2sbr!4v1462134646298" width="331" height="369" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </article>
                <article class="column">
                    <div class="cms-editable" id="contato-adress">
                        <h4>Fale conosco</h4>
                        <p>(48) 3015-6303 <br>
                        (48) 8429-3563 (24h) <br>
                        ​E-mail: ariela@arielarodrigues.adv.br</p>
     
                        <h4>Localização</h4>
                        <p>Rua Adhemar da Silva, n. 1.060, <br>
                        Sala 10, Kobrasol <br>
                        CEP 88.101-091 <br>
                        São José/SC</p>
                    </div>
                </article>
                <article class="column">
                    <form action="email.php" id="contactForm" method="POST">
                        <label for="name">Nome</label>
                        <input type="text" name="name" id="name">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email">
                        <label for="message">Mensagem</label>
                        <textarea name="message" id="message" cols="30" rows="10"></textarea>
                        <button type="submit">Enviar</button>
                    </form>
                    <div id="contactResponse"></div>
                </article>
            </div>
            <div class="row">
                <div class="column">
                    <hr>
                    <p class="cms-editable" id="footer">Ariela Rodrigues - Advocacia Criminal - Todos os direitos reservados - 2016</p>
                </div>
            </div>
        </div>
    </section>
  
    </div>
    
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="public/js/scripts.js"></script>
    <script src="public/js/rss.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-78182701-1', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>