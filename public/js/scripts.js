// Fix menu when scroll
var header = $("#wrap");
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
       if (scroll >= 550) {
          header.addClass("fixed");
        } else {
          header.removeClass("fixed");
        }
});

// Smooth scroll
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

// Send email
 $("#contactForm").submit(function(event) 
 {
     event.preventDefault();

     var $form = $( this ),
         $submit = $form.find( 'button[type="submit"]' ),
         name_value = $form.find( 'input[name="name"]' ).val(),
         email_value = $form.find( 'input[name="email"]' ).val(),
         message_value = $form.find( 'textarea[name="message"]' ).val(),
         url = $form.attr('action');

     var posting = $.post( url, { 
                       name: name_value, 
                       email: email_value, 
                       message: message_value 
                   });

     posting.done(function( data )
     {
         //$( "#contactResponse" ).html(data);
         $submit.text('E-mail enviado.');
         $submit.attr("disabled", true);
     });
});