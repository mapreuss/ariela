# README #

# Dependences

* SASS
* Bower
* [Milligram](https://github.com/milligram/milligram) 

# Local development

* fork and download repo
* `bower install`
* `sass resources/scss/site.scss:public/css/site.css`
* run `index.html` in a php enviroment

# Deploy

Send to a php enviroment (we use FTP today)

* /public/*
* index.html
* email.php

Or connect via SSH and follow the steps above (local development).

# Changing tumblr theme

* edit locally both `tumblr-theme.html` and `resources/scss/blog.scss`
* `sass resources/scss/blog.scss:public/css/blog.css`
* ftp upload `public/css/blog.css`
* change tumblr HTML theme

Don't forget to commit changes to HTML theme. 

# Editing content

We use PageLime to change content. Go to http://cms.pagelime.com and login with your user and password.

Create new modules? Add `cms-editable`class and an unique id to make a module cms editable.

# Editing style

We use SASS to stylesheets.

* edit `resources/scss/site.scss`
* `sass resources/scss/site.scss:public/css/site.css`
* ftp upload `public/css/site.css`

# E-mail box

We use a simple php and ajax script to send mail.

Change messages, recipient and subject at `email.php`. Change behaviour at `public/js/scripts.js`.

# Problems?

Contact me. I'll be glad to help.
marta.preuss@gmail.com